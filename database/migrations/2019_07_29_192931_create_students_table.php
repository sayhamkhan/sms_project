<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 64);
            $table->string('last_name', 64);
            $table->string('gender', 64);
            $table->text('present_address');
            $table->bigInteger('phone');
            $table->string('email', 64);
            $table->string('password', 255);
            $table->integer('class_id');
            $table->string('roll', 128);
            $table->string('dob', 32);
            $table->bigInteger('parent_phone');
            $table->text('permanent_address');
            $table->bigInteger('emergency_contact');
            $table->string('blood_group', 32);
            $table->string('status', 32)->default('inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
