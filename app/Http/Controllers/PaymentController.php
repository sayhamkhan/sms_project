<?php

namespace App\Http\Controllers;

use App\Models\Fee;
use App\Models\payment;
use App\Models\Salary;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function paymentsList()
    {
        $datas=payment::with('student')->with('total')->get();
        //dd($datas);
        return view('Backend.Payments.paymentsList', compact('datas'));
    }
    public function createPayments()
    {
        $student=Student::all();
        $datas=Fee::all();
        return view('Backend.Payments.createPayments', compact('datas', 'student'));
    }
    public function jsonAddPaymentValue(Request $request)
    {
        $data=Fee::select('type','id', 'amount')->where('id',$request->id)->take(100)->first();
        return response()->json($data);
    }
    public function createPaymentProcess(Request $request)
    {
        $validatedData = $request->validate([
            'student_id' => 'required',
            'total_amount' => 'required',
            'description' => 'required',
        ]);
        //dd($request->all());
        $test=payment::create([
            'student_id'=> $request->input('student_id'),
            'total_amount'=> $request->input('total_amount'),
            'description'=> $request->input('description'),
            'year'=> Carbon::now()->format('Y'),
        ]);
        $adminId=auth()->user()->id;
        $currAmount=$test->totalAmount['amount'];
        $admin=User::where('id', $adminId)->first();
        $totalAmount=($admin->amount)+$currAmount;
        $amount=User::where('id', $adminId)->update([
            'amount'=> $totalAmount,
        ]);
        //dd($totalAmount);
        session()->flash('message', 'Payment Create Successfully');
        return redirect()->route('paymentsList');
    }
    public function paymentDelete($id)
    {
        payment::find($id)->delete();
        session()->flash('message', 'Payment Delete Successfully');
        return redirect()->back();
    }
    public function editSalary($id)
    {
        $salary=Salary::with('teacher')->find($id);
        //dd($salary);
        return view('Backend.Payments.editSalary', compact('datas', 'salary'));
    }
    public function editSalaryProcess(Request $request, $id)
    {
        $validatedData = $request->validate([
            'amount' => 'required',
        ]);
        $salary=Salary::with('teacher')->find($id);
        $date=Carbon::now()->format('m-Y');
        $create=Salary::where('id', $id)->update([
            'amount'=> $request->input('amount'),
            'date'=> $date,
        ]);
        $adminId=Auth::guard('admin')->user()->id;
        $currAmount=$request->input('amount');
        $admin=User::where('id', $adminId)->first();
        $finalAmount=($admin->amount)+($salary->amount);
        //dd($salary->amount);
        $totalAmount=$finalAmount-$currAmount;
        $amount=User::where('id', $adminId)->update([
            'amount'=> $totalAmount,
        ]);
        session()->flash('message', 'Salary Edit Successfully For This Month');
        return redirect()->route('salaryList');
    }
    public function feeList()
    {
        $datas=Fee::all();
        return view('Backend.Payments.feeList', compact('datas'));
    }
    public function feeDelete($id)
    {
        Fee::find($id)->delete();
        session()->flash('message', 'Fee Delete Successfully');
        return redirect()->back();
    }
    public function createFees(Request $request)
    {
        $validatedData = $request->validate([
            'amount' => 'required',
            'type' => 'required || unique:fees',
        ]);
        $test=Fee::create([
            'type'=> $request->input('type'),
            'amount'=> $request->input('amount'),
        ]);
        return redirect()->back();
    }
    public function salaryList()
    {
        $datas=Salary::all();
        //dd($datas);
        return view('Backend.Payments.salaryList', compact('datas'));
    }
    public function createSalary()
    {
        $datas=Teacher::all();
        //dd($datas);
        return view('Backend.Payments.createSalary', compact('datas'));
    }
    public function createSalaryProcess(Request $request)
    {
        $validatedData = $request->validate([
            'teacher_id' => 'required',
            'amount' => 'required',
        ]);
        $teacherId=$request->input('teacher_id');
        $date=Carbon::now()->format('m-Y');
        $salary=Salary::where('teacher_id', $teacherId)->where('date', $date)->first();
        //dd($salary);
        if (!empty($salary)){
            session()->flash('message', 'Salary Already Given For This Month');
            return redirect()->route('salaryList');
        }
        $test=Salary::create([
            'teacher_id'=> $request->input('teacher_id'),
            'amount'=> $request->input('amount'),
            'date'=> $date,
        ]);

        $adminId=Auth::guard('admin')->user()->id;
        //dd($adminId);
        $currAmount=$request->input('amount');
        $admin=User::where('id', $adminId)->first();
        $totalAmount=($admin->amount)-$currAmount;
        $amount=User::where('id', $adminId)->update([
            'amount'=> $totalAmount,
        ]);
        session()->flash('message', 'Salary Given Successfully For This Month');
        return redirect()->back();
    }
}
