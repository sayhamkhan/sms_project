<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('index');
    }
    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('teacherLogin');
    }
}
