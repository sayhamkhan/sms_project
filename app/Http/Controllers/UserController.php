<?php

namespace App\Http\Controllers;

use App\Models\Classe;
use App\Models\Mark;
use App\Models\payment;
use App\Models\Student;
use App\Models\Teacher;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use PDF;

class UserController extends Controller
{
    public function adminIndex()
    {
        return view('Backend.index');
    }

    public function studentList()
    {
        $datas = Student::with('class')->get();
        //dd($datas);
        $class = Classe::all();
        return view('Backend.User.studentList', compact('datas', 'class'));
    }

    public function createStudent(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'present_address' => 'required',
            'phone' => 'required || min:10',
            'email' => 'required|| email || unique:students,email',
            'password' => 'required',
            'class_id' => 'required',
            'dob' => 'required',
            'parent_phone' => 'required || min:10',
            'permanent_address' => 'required',
            'emergency_contact' => 'required || min:10',
            'blood_group' => 'required',
        ]);
        $roll = $request->roll;
        $class = $request->class_id;
        $classRoll = Student::where('class_id', $class)->latest()->first();
        $oldRol = ($classRoll['roll']);
        //$newRoll=$oldRol+1;
        //dd($classRoll);
        if (!empty($classRoll)) {
            $newRoll = $oldRol + 1;
        } else {
            $newRoll = $oldRol;
        }
        //dd($newRoll);
        $test = Student::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'present_address' => $request->input('present_address'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'roll' => $newRoll,
            'class_id' => $request->input('class_id'),
            'dob' => $request->input('dob'),
            'parent_phone' => $request->input('parent_phone'),
            'permanent_address' => $request->input('permanent_address'),
            'emergency_contact' => $request->input('emergency_contact'),
            'blood_group' => $request->input('blood_group'),
        ]);
        session()->flash('message', 'Student Admitted Successfully');
        return redirect()->back();

    }

    public function teacherList()
    {
        $datas = Teacher::all();
        //dd($datas);
        //$class=Classe::all();
        return view('Backend.User.teacherList', compact('datas'));
    }

    public function createTeacher(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'present_address' => 'required',
            'phone' => 'required || min:11',
            'email' => 'required|| email || unique:students,email',
            'password' => 'required',
        ]);
        $test = Teacher::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'gender' => $request->input('gender'),
            'present_address' => $request->input('present_address'),
            'password' => Hash::make($request->input('password')),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
        ]);
        session()->flash('message', 'Teacher Added Successfully');
        return redirect()->back();
    }


    public function editTeacher($id)
    {
        $teacher_info = Teacher::where('id', $id)->first();
        $teacher = Teacher::all();
        return view('Backend.User.teacherEdit', compact('teacher_info', 'teacher'));

    }
public function editTeacherProcess(Request $request, $id){

    $validatedData = $request->validate([
        'first_name' => 'required',
        'last_name' => 'required',
        'present_address' => 'required',
        'phone' => 'required',


    ]);
    Teacher::where('id', $id)->update([
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
        'present_address' => $request->input('present_address'),
        'phone' => $request->input('phone'),

    ]);

    $teacherNames = Teacher::all();
    session()->flash('message', 'Teacher Updated Successfully');
    return redirect()->route('teacherList', compact('teacherNames'));

}

    public function teacherDelete($id)
    {
        $teacher = Teacher::find($id)->delete();
        session()->flash('message', 'Teacher Delete Successfully');
        return redirect()->back();
    }

    //Mark Publish Start Here
    public function markPublishList()
    {
        $marks = Mark::with('class')->with('student')->with('course')->get();
        //dd($marks);
        return view('Backend.mark.publishList', compact('marks'));
    }

    public function publishMark($id)
    {

        $test = Mark::where('id', $id)->update([
            'status' => "publish",
        ]);

        return redirect()->back();
    }
    public function editStudent($id)
    {
        $student=Student::find($id);
        $class=Classe::all();
        return view('Backend.User.editStudent', compact('student', 'class'));
    }
    public function editStudentProcess(Request $request, $id)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'present_address' => 'required',
            'phone' => 'required || min:10',
            'class_id' => 'required',
            'dob' => 'required',
            'parent_phone' => 'required || min:10',
            'permanent_address' => 'required',
            'emergency_contact' => 'required || min:10',
            'blood_group' => 'required',
        ]);
        $roll=$request->roll;
        $class=$request->class_id;
        $classRoll=Student::where('class_id', $class)->latest()->first();
        $oldRol=($classRoll['roll']);
        if (!empty($classRoll)){
            $newRoll=$oldRol+1;
        }else{
            $newRoll=$oldRol;
        }
        $test=Student::where('id', $id)->update([
            'first_name'=> $request->input('first_name'),
            'last_name'=> $request->input('last_name'),
            'gender'=> $request->input('gender'),
            'present_address'=> $request->input('present_address'),
            'phone'=> $request->input('phone'),
            'roll'=> $newRoll,
            'class_id'=> $request->input('class_id'),
            'dob'=> $request->input('dob'),
            'parent_phone'=> $request->input('parent_phone'),
            'permanent_address'=> $request->input('permanent_address'),
            'emergency_contact'=> $request->input('emergency_contact'),
            'blood_group'=> $request->input('blood_group'),
        ]);
        session()->flash('message', 'Student Info Update Successfully');
        return redirect()->route('studentList');
    }
    public function deleteStudent($id)
    {
        Student::find($id)->delete();
        session()->flash('message', 'Student Delete Successfully');
        return redirect()->back();

    }

    public function paymentPrint($id)
    {
        $downloadPdf =payment::with('student')->with('total')->find($id);
        //dd($downloadPdf);
       $pdf = PDF::loadView('frontend.print.pdfStudent', compact('downloadPdf'));
       return $pdf->setPaper('a4')->stream();
    }

}
