<?php

namespace App\Http\Controllers;

use App\Models\Classe;
use App\Models\Classteacher;
use App\Models\Course;
use App\Models\Formmaster;
use App\Models\Section;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    public function classList()
    {
        $classNames = Classe::all();
        return view('Backend.Class.classList', compact('classNames'));
    }

    public function createClasses(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required || unique:classes,name',
        ]);
        $test = Classe::create([
            'name' => $request->input('name'),
        ]);
        session()->flash('message', 'Salary Given Successfully For This Month');
        return redirect()->back();
    }

    public function editCls($id)
    {
        $classes_info = Classe::where('id', $id)->first();
        $class = Classe::all();
        return view('Backend.Class.classEdit', compact('classes_info', 'class'));
    }


    public function editClassesProcess(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);
        Classe::where('id', $id)->update([
            'name' => $request->input('name')
        ]);

        $classNames = Classe::all();
        session()->flash('message', 'Class Updated Successfully');
        return redirect()->route('classList', compact('classNames'));

    }

    public function clsDelete($id)
    {
        $class = Classe::find($id)->delete();
        return redirect()->back();
    }

    public function courseList()
    {
        $datas = Course::with('class')->latest()->get();
        $cls=Classe::all();
        return view('Backend.Class.courseList', compact('datas', 'cls'));
    }

    public function createCourse(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required',
            'class_id' => 'required',
        ]);

        $check = $request->name;
        $checkClass = $request->class_id;
        $check_info = Course::where('name', $check)->where('class_id', $checkClass)->first();
        if ($check_info == null) {


            $test = Course::create([
                'name' => $request->input('name'),
                'class_id' => $request->input('class_id'),
            ]);
            session()->flash('message', 'Course Created Successfully');
            return redirect()->back();
        } else {
            session()->flash('message', 'Course Already Added');
            return redirect()->back();
        }
    }







    public function updateCourse($id)
    {
        $course_info = Course::where('id', $id)->first();
        $course = Course::all();
        return view('Backend.Class.courseEdit', compact('course_info', 'course'));


    }


    public function editCourseProcess(Request $request, $id)
    {
        Course::where('id', $id)->update([
            'name' => $request->input('name')
        ]);

//        $courseNames = Course::all();
        $cor = Course::all();
        session()->flash('message', 'Course Updated Successfully');
        return redirect()->route('courseList', compact('cor'));


    }


    public function courseDelete($id)
    {
        Course::find($id)->delete();
        return redirect()->back();

    }


    public function sectionList()
    {

        $datas = Section::with('class')->get();
        $cls = Classe::all();
        return view('Backend.Class.sectionList', compact('datas', 'cls'));
    }

    public function createSection(Request $request)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'name' => 'required',
            'class_id' => 'required',
        ]);
        $check = $request->name;
        $checkClass = $request->class_id;
        $check_info = Section::where('name', $check)->where('class_id', $checkClass)->first();
//        dd($check_info);
        if($check_info==null){
            $test = Section::create([
                'name' => $request->input('name'),
                'class_id' => $request->input('class_id'),
            ]);
            session()->flash('message', 'Section Created Successfully');
            return redirect()->back();
        }else{

            session()->flash('message', 'Section Already Added');
            return redirect()->back();
        }
    }

    public function editSection($id)
    {
        $section_info = Section::where('id', $id)->first();
        $section = Section::all();
        return view('Backend.Class.sectionEdit', compact('section_info', 'section'));

    }

    public function editSectionProcess(Request $request, $id)
    {
        Section::where('id', $id)->update([
            'name' => $request->input('name')
        ]);

        $sec = Section::all();
        session()->flash('message', 'Section Updated Successfully');
        return redirect()->route('sectionList', compact('sec'));


    }

    public function secDelete($id)
    {
        $section = Section::find($id)->delete();
        return redirect()->back();

    }


    public function formMaster()
    {
        $datas = Formmaster::with('class')->with('teacher')->get();
        $cls = Classe::all();
        $teacher = Teacher::all();
        return view('Backend.Class.formMasterList', compact('datas', 'cls', 'teacher'));
    }

    public function assignFormMaster(Request $request)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'teacher_id' => 'required || unique:formmasters,teacher_id',
            'class_id' => 'required || unique:formmasters,class_id',
        ]);

        $classId = $request->input('class_id');
        $teacherId = $request->input('teacher_id');
        $test = Formmaster::where('class_id', $classId)->where('teacher_id', $teacherId)->first();
        //dd($test->id);
        if (empty($test)) {
            $test = Formmaster::create([
                'class_id' => $classId,
                'teacher_id' => $teacherId,
            ]);
        } else {
            Formmaster::where('id', $classId)->update([
                'class_id' => $classId,
                'teacher_id' => $teacherId,
            ]);
        }
        session()->flash('message', 'Form Master Assign Successfully');
        return redirect()->back();
    }

    public function classTeacher()
    {
        $datas = Classteacher::with('class')->with('teacher')->with('course')->get();
        $cls = Classe::all();
        $teacher = Teacher::all();
        $courses = Course::all();
        //dd($datas);
        return view('Backend.Class.classTeacherList', compact('datas', 'cls', 'teacher', 'courses'));
    }

    public function assignClassTeacher(Request $request)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'teacher_id' => 'required',
            'class_id' => 'required',
            'course_id' => 'required',
        ]);

        $classId = $request->input('class_id');
        $test = Classteacher::where('class_id', $classId)->first();
        //dd($test->id);
        if (empty($test)) {
            $test = Classteacher::create([
                'class_id' => $classId,
                'teacher_id' => $request->input('teacher_id'),
                'course_id' => $request->input('course_id'),
            ]);
        } else {
            Classteacher::where('id', $classId)->update([
                'class_id' => $classId,
                'teacher_id' => $request->input('teacher_id'),
                'course_id' => $request->input('course_id'),
            ]);
        }
        session()->flash('message', 'Class Teacher Assign Successfully');
        return redirect()->back();
    }
}
