<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected $guarded=[];
    public function student()
    {
        return $this->hasOne(Student::Class, 'id', 'student_id');
    }
    public function total()
    {
        return $this->hasOne(Fee::Class, 'id', 'total_amount');
    }
}
