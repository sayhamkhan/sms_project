<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    protected $guarded = [];

    public function class()
    {
        return $this->hasOne(Classe::Class, 'id', 'class_id');
    }
    public function student()
    {
        return $this->hasOne(Student::Class, 'id', 'student_id');
    }
    public function course()
    {
        return $this->hasOne(Course::Class, 'id', 'subject_id');
    }
}
