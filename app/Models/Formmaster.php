<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formmaster extends Model
{
    protected $guarded=[];

    public function class()
    {
        return $this->hasOne(Classe::Class, 'id', 'class_id');
    }
    public function teacher()
    {
        return $this->hasOne(Teacher::Class, 'id', 'teacher_id');
    }
}
