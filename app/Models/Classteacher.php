<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classteacher extends Model
{
    protected $guarded=[];

    public function class()
    {
        return $this->hasOne(Classe::Class, 'id', 'class_id');
    }
    public function teacher()
    {
        return $this->hasOne(Teacher::Class, 'id', 'teacher_id');
    }
    public function course()
    {
        return $this->hasOne(Course::Class, 'id', 'course_id');
    }
}
