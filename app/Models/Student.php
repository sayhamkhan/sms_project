<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    protected $guarded=[];
    public function class()
    {
        return $this->hasOne(Classe::Class, 'id', 'class_id');
    }

}
