@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    @if (session('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            {{ session('message')}}
                        </div>
                    @endif
                    <h4 class="card-title">Give Salary</h4>
                    <form action="{{route('createSalaryProcess')}}" method="post" role="form">
                        @CSRF
                        <div class="form-group">
                            <label for="teacher_id">Teacher Name:</label>
                            <select class="form-control" name="teacher_id" id="teacher_id">
                                @foreach($datas as $data)
                                    <option value="{{$data->id}}">{{$data->first_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{--<div class="form-group row">--}}
                        {{--<label for="student_id" class="col-sm-3 col-form-label">Student ID</label>--}}
                        {{--<div class="col-sm-9">--}}
                        {{--$student<input type="text" class="form-control" id="student_id" name="student_id" placeholder="Fees Type">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group row">
                            <label for="amount" class="col-sm-3 col-form-label">Amount</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="amount" name="amount" placeholder="Salary Amount">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


