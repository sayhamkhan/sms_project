@extends('Backend.master')
@section('content')
<div class="row">
    <div class="col-lg-12 grid-margin">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('status'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                    {{ session('status')}}
                </div>
                @endif
                <h4 class="card-title">Give Salary</h4>
                <form action="{{route('editSalaryProcess', $salary->id)}}" method="post" role="form">
                    @CSRF
                    <div class="form-group row">
                        <label for="teacher_id" class="col-sm-3 col-form-label">Teacher Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="amount" name="amount" value="{{$salary->teacher->first_name}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="amount" class="col-sm-3 col-form-label">Amount</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" id="amount" name="amount" value="{{$salary->amount}}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

@stop


