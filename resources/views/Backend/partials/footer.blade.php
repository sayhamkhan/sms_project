</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
              <a href="#" target="_blank">QSS</a>
              . All rights reserved.
            </span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
{{--Ajax Data Load Start--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js  "></script>
{{--Ajax Data Load End--}}
<script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>

<script src="{{asset('js/off-canvas.js')}}"></script>
<script src="{{asset('js/misc.js')}}"></script>

<script src="{{asset('js/jquery.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery-1.12.0.min.js')}}"><\/script>')</script>--}}
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- bootstrap js-->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('js/dashboard.js')}}"></script>
<!-- End custom js for this page-->
<script type="text/javascript">
    // console.log("hmm its change");
    $(document).ready(function(){
        // Category Name Change Start
        $(document).on('change','#paymentType',function(){
            var paymentId=$(this).val();
            var div=$(this).parent();
            var paymentValue=" ";
            // console.log("BooBoo Test");
            // console.log(paymentId);
            $.ajax({
                type:'get',
                url:'{!!URL::to('jsonAddPaymentValue')!!}',
                data:{'id':paymentId},
                success:function(data){
                    console.log(data.amount);
                    // paymentValue+= $("#paymentValue").append('<input type="text" class="form-control" id="paymentValue" value="'+data.amount+'" name="paymentValue" placeholder="Amount" readonly>');
                    $("#paymentValue").empty().append('<input type="text" class="form-control" id="paymentValue" value="'+data.amount+'" name="paymentValue" placeholder="Amount" readonly>');
                    // if(data.length>0)
                    // {
                    //     for(let i=0;i<data.length;i++){
                    //         op+='<option value="'+data[i].id+'">'+data[i].cate_name+'</option>';
                    //         $("#paymentValue").append('<input type="text" class="form-control" id="paymentValue" value="'+data[i].amount+'" name="paymentValue" placeholder="Amount" readonly>');
                    //         $("#paymentValue").append('<option value="'+data[i].amount+'">'+data[i].amount+'</option>');
                    //     }
                    // }else {
                    //     let select = document.getElementById("paymentValue");
                    //     select.options.length=0;
                    // }
                },
                error:function(){
                }
            });
        });
        //Category Name Change End
    });
</script>
@stack('scripts')


</body>

</html>
