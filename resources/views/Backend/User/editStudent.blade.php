@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    @if (session('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            {{ session('message')}}
                        </div>
                    @endif
                    <h1 class="card-title">Edit Student</h1>
                        <form action="{{route('editStudentProcess', $student->id)}}" method="post">
                            @CSRF
                            <div class="form-group row">
                                <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{$student->first_name}}">
                                </div>
                                <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{$student->last_name}}">
                                </div>
                            </div>
                            <div class="form-group d-flex pay_select">
                                <label for="gender">Select Gender:</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value="">---Select Gender---</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="others">Others</option>
                                </select>
                            </div>
                            <div class="form-group d-flex pay_select">
                                <label for="class_id">Select Class</label>
                                <select class="form-control" name="class_id" id="class_id">
                                    <option value="">---Select Class---</option>
                                    @foreach($class as $data)
                                        <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label for="present_address" class="col-sm-2 col-form-label">Present Address</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="present_address" name="present_address" value="{{$student->present_address}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="phone" name="phone" value="{{$student->phone}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                {{--<label for="roll" class="col-sm-2 col-form-label">ID No</label>--}}
                                {{--<div class="col-sm-4">--}}
                                {{--<input type="number" class="form-control" id="roll" name="roll" placeholder="ID No">--}}
                                {{--</div>--}}
                                <label for="dob" class="col-sm-2 col-form-label">Date of Birth</label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" id="dob" name="dob" value="2001-01-01">
                                </div>
                                <label for="blood_group" class="col-sm-2 col-form-label">Blood Group</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="blood_group" name="blood_group" value="{{$student->blood_group}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="parent_phone" class="col-sm-2 col-form-label">Parent Phone</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="parent_phone" name="parent_phone" value="{{$student->parent_phone}}">
                                </div>
                                <label for="emergency_contact" class="col-sm-2 col-form-label">Emergency Contact</label>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="emergency_contact" name="emergency_contact" value="{{$student->emergency_contact}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="permanent_address" class="col-sm-3 col-form-label">Permanent Address</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="permanent_address" name="permanent_address" value="{{$student->permanent_address}}">
                                </div>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                </div>
            </div>
        </div>
    </div>
@stop
