@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    @if (session('message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            {{ session('message')}}
                        </div>
                    @endif
                    <h1 class="card-title">Student Lists</h1>
                    <!-- Button trigger modal -->
                        <a href="{{ URL::to('/customers/pdf') }}">Export PDF</a>
                    <button type="button" class="btn btn-success btn-block"  data-toggle="modal" data-target="#exampleModal">Add Student
                        <i class="mdi mdi-plus"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog_lg" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Student</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('createStudent')}}" method="post">
                                        @CSRF
                                        <div class="form-group row">
                                            <label for="first_name" class="col-sm-2 col-form-label">First Name</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                                            </div>
                                            <label for="last_name" class="col-sm-2 col-form-label">Last Name</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group d-flex pay_select">
                                            <label for="gender">Select Gender:</label>
                                            <select class="form-control" name="gender" id="gender">
                                                    <option value="">---Select Gender---</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                    <option value="others">Others</option>
                                            </select>
                                        </div>
                                        <div class="form-group d-flex pay_select">
                                            <label for="class_id">Select Class</label>
                                            <select class="form-control" name="class_id" id="class_id">
                                                <option value="">---Select Class---</option>
                                                @foreach($class as $data)
                                                <option value="{{$data->id}}">{{$data->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group row">
                                            <label for="present_address" class="col-sm-2 col-form-label">Present Address</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="present_address" name="present_address" placeholder="Present Address">
                                            </div>
                                            <label for="password" class="col-sm-2 col-form-label">Password</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-4">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                            </div>
                                            <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{--<label for="roll" class="col-sm-2 col-form-label">ID No</label>--}}
                                            {{--<div class="col-sm-4">--}}
                                                {{--<input type="number" class="form-control" id="roll" name="roll" placeholder="ID No">--}}
                                            {{--</div>--}}
                                            <label for="dob" class="col-sm-2 col-form-label">Date of Birth</label>
                                            <div class="col-sm-4">
                                                <input type="date" class="form-control" id="dob" name="dob" value="2001-01-01">
                                            </div>
                                            <label for="blood_group" class="col-sm-2 col-form-label">Blood Group</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="blood_group" name="blood_group" placeholder="Blood Group">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="parent_phone" class="col-sm-2 col-form-label">Parent Phone</label>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" id="parent_phone" name="parent_phone" placeholder=Parent_Phone">
                                            </div>
                                            <label for="emergency_contact" class="col-sm-2 col-form-label">Emergency Contact</label>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" id="emergency_contact" name="emergency_contact" placeholder="Emergency Contact">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="permanent_address" class="col-sm-3 col-form-label">Permanent Address</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="permanent_address" name="permanent_address" placeholder="Permanent Address">
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive" id="dvContainer">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Phone
                                </th>
                                <th>
                                    Class
                                </th>
                                <th>
                                    Roll
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            ?>
                            @foreach($datas as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->first_name}} {{$data->last_name}}
                                    </td>
                                    <td>
                                        {{$data->phone}}
                                    </td>
                                    <td>
                                        {{$data->class->name}}
                                    </td>
                                    <td>
                                        {{$data->roll}}
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="{{route('editStudent', $data->id)}}">
                                            <span class="icon edit_icon">
                                                <i class="fa fa-edit"></i>
                                            </span>
                                        </a>
                                        <a href="{{route('deleteStudent', $data->id)}}">
                                            <span class="icon remove_icon">
                                                <i class="fa fa-trash"></i>
                                            </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
<script type="text/javascript">
    $("#btnPrint").live("click", function () {
        var divContents = $("#dvContainer").html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>DIV Contents</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });
</script>
