@extends('Backend.master')

@section('content')

    @include('frontend.partials.sidebar')

{{--@stop--}}

{{--@section('contant')--}}



    <div class="row">

        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">Edit Teacher</h1>
                            <form class="forms-sample" action="{{route('editTeacherProcess', $teacher_info->id)}}" method="POST"
                                  role="form">
                                @method('put')
                                @csrf
                                <div class="form-group">
                                    <label for="className">Teacher Name</label>
                                    <input type="text" name="first_name" class="form-control"
                                           value="{{$teacher_info->first_name}}" id="className"
                                           placeholder="Enter Class Name">
                                </div>
                                <div class="form-group">
                                    <label for="className">Teacher Name</label>
                                    <input type="text" name="last_name" class="form-control"
                                           value="{{$teacher_info->last_name}}" id="className"
                                           placeholder="Enter Class Name">
                                </div>

                                <div class="form-group">
                                    <label for="className">Address</label>
                                    <input type="text" name="present_address" class="form-control"
                                           value="{{$teacher_info->present_address}}" id="className"
                                           placeholder="Enter Class Name">
                                </div>

                                <div class="form-group">
                                    <label for="className">Phone</label>
                                    <input type="text" name="phone" class="form-control"
                                           value="{{$teacher_info->phone}}" id="className"
                                           placeholder="Enter Class Name">
                                </div>


                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-danger" type="reset">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@stop
