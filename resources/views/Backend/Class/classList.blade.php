@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            {{ session('message')}}
                        </div>
                    @endif
                    <h4 class="card-title">Class Lists</h4>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-block"  data-toggle="modal" data-target="#exampleModal">Add Class
                        <i class="mdi mdi-plus"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Class</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('createClasses')}}" method="post">
                                        @CSRF
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Class Name">
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            ?>
                            @foreach($classNames as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->name}}
                                    </td>
                                    <td>
                                        {{$data->status}}
                                    </td>

                                    <td style="text-align: center;">
                                        <a href="{{route('editCls', $data->id)}}">
                                            <span class="icon edit_icon">
                                                <i class="fa fa-edit"></i>
                                            </span>
                                        </a>


                                        <a href="{{route('clsDelete', $data->id)}}">
                                            <span class="icon remove_icon">
                                                 <i class="fa fa-trash"></i>
                                            </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
