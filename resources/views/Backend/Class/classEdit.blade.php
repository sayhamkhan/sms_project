@extends('Backend.master')

@section('content')

    @include('frontend.partials.sidebar')

{{--@stop--}}

{{--@section('contant')--}}



    <div class="row">

        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title">Edit Class</h1>
                            <form class="forms-sample" action="{{route('editClassesProcess', $classes_info->id)}}" method="POST"
                                  role="form">
                                @method('put')
                                @csrf
                                <div class="form-group">
                                    <label for="className">Class Name</label>
                                    <input type="text" name="name" class="form-control"
                                           value="{{$classes_info->name}}" id="className"
                                           placeholder="Enter Class Name">
                                </div>
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-danger" type="reset">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@stop
