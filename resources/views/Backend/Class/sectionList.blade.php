@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @foreach ($errors->all() as $error)
                        <p class="alert alert-danger">{{ $error }}</p>
                    @endforeach
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            {{ session('status')}}
                        </div>
                    @endif
                    <h4 class="card-title">Section Lists</h4>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-success btn-block" data-toggle="modal"
                            data-target="#exampleModal">Add Section
                        <i class="mdi mdi-plus"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Section</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="{{route('createSection')}}" method="post">
                                        @CSRF
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="name" name="name"
                                                       placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="form-group d-flex pay_select">
                                            <label for="class_id">Select Class:</label>
                                            <select class="form-control" id="class_id" name="class_id">
                                                <option value="">Select a Class Name</option>
                                                @foreach($cls as $data)
                                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Section Name
                                </th>
                                <th>
                                    Class Name
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($datas as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->name}}
                                    </td>
                                    <td>
                                        {{$data->class->name}}
                                    </td>
                                    <td>
                                        {{$data->status}}
                                    </td>

                                    <td style="text-align: center;">
                                        <a href="{{route('editSection', $data->id)}}">
                                            <span class="icon edit_icon">
                                                 <i class="fa fa-edit"></i>
                                             </span>
                                        </a>

                                        <a href="{{route('secDelete', $data->id)}}">
                                        <span class="icon remove_icon">
                                             <i class="fa fa-trash"></i>
                                         </span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
