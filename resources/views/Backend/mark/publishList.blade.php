@extends('Backend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Class Lists</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                   Course Name
                                </th>
                                <th>
                                   Student Name
                                </th>
                                <th>
                                   Class Name
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    First Term
                                </th>
                                <th>
                                    Mid Term
                                </th>
                                <th>
                                    Final
                                </th>
                                <th>
                                    Year
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            ?>
                            @foreach($marks as $data)
                                <tr>
                                    <td class="font-weight-medium">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{$data->course->name}}
                                    </td>
                                    <td>
                                        {{$data->student->first_name}}
                                    </td>
                                    <td>
                                        {{$data->class->name}}
                                    </td>
                                    <td>
                                        {{$data->first_term}}
                                    </td>
                                    <td>
                                        {{$data->mid_term}}
                                    </td>
                                    <td>
                                        {{$data->final_term}}
                                    </td>
                                    <td>
                                        {{$data->year}}
                                    </td>

                                    <td style="text-align: center;">
                                        @if ($data->status=="pending")
                                            <a href="{{route('publishMark', $data->id)}}">
                                            <span class="icon edit_icon">
                                                <i class="fa fa-thumbs-up"></i>
                                            </span>
                                            </a>
                                        @else
                                            <a href="">
                                            <span class="icon edit_icon" style="color:darkred">
                                                <i class="fa fa-thumbs-up"></i>
                                            </span>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
