@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.teacher.navbar.navbar')
                <form method="POST" action="{{route('submitmarks')}}" role="form">
                    @method('put')
                    @csrf
                    <div class="col-md-10 col_center select_class_all">
                        <div class="form-group">
                            <label for="subject_id">Select Subject</label>
                            <input type="hidden" name="class_id" value="{{$class_id}}">
                            <select class="form-control" name="subject_id" id="subject_id">
                                <option value="0">Select Subject Name</option>
                                @foreach($datas as $data)
                                    <option value="{{$data->id}}">{{$data->course->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>
                                        Roll
                                    </th>
                                    <th>
                                        First Term
                                    </th>
                                    <th>
                                        Mid Term
                                    </th>
                                    <th>
                                        Final Term
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="student_id" id="class_id" required>
                                                    <option value="0">Select Roll</option>
                                                    @foreach($students as $data)
                                                        <option value="{{$data->id}}">{{$data->roll}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group" id="ft_mrk">
                                                <input type="number" class="form-control" name="first_term"  min="1" max="100" placeholder="Enter First Term Marks">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group"  id="mt_mrk">
                                                <input type="number" class="form-control" name="mid_term"  min="1" max="100" placeholder="Enter Mid Term Marks">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group" id="fi_mrk">
                                                <input type="number" class="form-control" name="final_term"  min="1" max="100" placeholder="Enter Final Marks">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button type="submit" class="btn btn-lg btn-success float-right">Submit</button>
                    </div>
                </form>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
