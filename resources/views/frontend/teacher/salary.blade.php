@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.teacher.navbar.navbar')
                @if ($data!==null)
                    <div class="hed">
                        <h1 class="text-center">You got your this month salary.</h1>
                    </div>
                @else
                    <div class="hed">
                        <h1 class="text-center">You still not get your this month salary.</h1>
                    </div>
                @endif
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
