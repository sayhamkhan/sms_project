@extends('frontend.master')
@section('content')
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    @include('frontend.teacher.navbar.navbar')
                    <h4 class="card-title">Student Lists</h4>
                    @if ($attenCount==null)
                        <div class="table-responsive">
                            <form action="{{route('takeAttendanceProcess')}}" method="post">
                                @CSRF
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Roll
                                        </th>
                                        <th>
                                            Attendance
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    ?>
                                    @foreach($datas as $data)
                                        <tr>
                                            <td class="font-weight-medium">
                                                {{$i++}}
                                            </td>
                                            <td>
                                                <input type="hidden" name="id[]" value="{{$data->id}}">
                                                {{$data->first_name}} {{$data->last_name}}
                                            </td>
                                            <td>
                                                {{$data->roll}}
                                            </td>
                                            <td>
                                                <div class="form-group d-flex pay_select">
                                                    <select class="form-control" name="attendance[]" id="attendance">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </form>
                        </div>
                    @else
                        <h4 class="card-title">Today's Attendance Already Taken.</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
