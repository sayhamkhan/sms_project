<div class="row w-100 pt-lg-5">
    <div class="col-lg-12 mx-auto teacher_btn">
        <div class="btn-group btn-group-lg" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-primary">
                <a href="{{route('takeAttendance')}}">Attendance</a>
            </button>
            <button type="button" class="btn btn-info">
                <a href="{{route('teacherProfile')}}">Profile</a>
            </button>
            <button type="button" class="btn btn-success">
                <a href="{{route('showAttendance')}}">Show Attendance</a>
            </button>
            <button type="button" class="btn btn-dark">
                <a href="{{route('teacherClassList')}}">Class</a>
            </button>
            <button type="button" class="btn btn-success">
                <a href="{{route('selectClass')}}">Submit Marks</a>
            </button>
            <button type="button" class="btn btn-success">
                <a href="{{route('teacherSalary')}}">Salary</a>
            </button>
        </div>
    </div>
</div>
@foreach ($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
@if (session('message'))
    <div class="alert alert-success alert-dismissable">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
        {{ session('message')}}
    </div>
@endif
