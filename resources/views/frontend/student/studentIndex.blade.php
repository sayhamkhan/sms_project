@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.student.navbar.navbar')
                <div class="row">
                    <div class="col-md-6 col_center info_bg">
                        <h1 class="text-center">Hi, {{$info->first_name}} {{$info->last_name}}</h1>
                        <ul class="all_info">
                            <li class="single_info">
                                <span class="info_title">Phone:</span>
                                <span class="info_common">{{$info->phone}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Email:</span>
                                <span class="info_common">{{$info->email}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Class:</span>
                                <span class="info_common">{{$info->class_id}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Roll:</span>
                                <span class="info_common">{{$info->roll}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Date of Birth:</span>
                                <span class="info_common">{{$info->dob}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Blood Group:</span>
                                <span class="info_common">{{$info->blood_group}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Emergency Contact:</span>
                                <span class="info_common">{{$info->emergency_contact}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Gender:</span>
                                <span class="info_common">{{$info->gender}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Parent Phone:</span>
                                <span class="info_common">{{$info->parent_phone}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Present Address:</span>
                                <span class="info_common">{{$info->present_address}}</span>
                            </li>
                            <li class="single_info">
                                <span class="info_title">Permanent Address:</span>
                                <span class="info_common">{{$info->permanent_address}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
