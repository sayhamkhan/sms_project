@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.student.navbar.navbar')
                <div class="row">
                    <div class="col-lg-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Mark's of the Students</h4>
                                @if ($data==null)
                                    <div class="hed">
                                        <h1 class="text-center">Your Exam Result is Not Publish Yet.</h1>
                                    </div>
                                @else
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>
                                                Class Name
                                            </th>
                                            <th>
                                                Student Name
                                            </th>
                                            <th>
                                                First Term
                                            </th>
                                            <th>
                                                Mid Term
                                            </th>
                                            <th>
                                                Final
                                            </th>
                                            <th>
                                                Grade
                                            </th>
                                            <th>
                                                Year
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    {{$data->class->name}}
                                                </td>
                                                <td>
                                                    {{$data->student->first_name}} {{$data->student->last_name}}
                                                </td>
                                                <td>
                                                    {{$data->first_term}}
                                                </td>
                                                <td>
                                                    {{$data->mid_term}}
                                                </td>
                                                <td>
                                                    {{$data->final_term}}
                                                </td>
                                                <td>
                                                    @switch($totalMarks)
                                                        @case($totalMarks>=80)
                                                        A+
                                                        @break
                                                        @case($totalMarks>=70)
                                                        A
                                                        @break
                                                        @case($totalMarks>=60)
                                                        A-
                                                        @break
                                                        @case($totalMarks>=50)
                                                        B
                                                        @break
                                                        @case($totalMarks>=40)
                                                        C
                                                        @break
                                                        @default
                                                        F
                                                        @break
                                                    @endswitch
                                                </td>
                                                <td>
                                                    {{$data->year}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
