@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.student.navbar.navbar')
                @if ($totalAttendance==null)
                    <div class="hed">
                        <h1 class="text-center">Today's Attendance not taken yet.</h1>
                    </div>
                @else
                    <div class="row">
                        <div class="col-lg-4 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body atd_card">
                                    <div class="clearfix">
                                        <div class="text-center">
                                            <h1 class="mb-0 text-center">Total Present</h1>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-center mb-0">{{$totalPresent}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body atd_card">
                                    <div class="clearfix">
                                        <div class="text-center">
                                            <h1 class="mb-0 text-center">Total Absent</h1>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-center mb-0">{{$totalAttendance-$totalPresent}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body atd_card">
                                    <div class="clearfix">
                                        <div class="text-center">
                                            <h1 class="mb-0 text-center">Total Class</h1>
                                            <div class="fluid-container">
                                                <h3 class="font-weight-medium text-center mb-0">{{$totalAttendance}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
