@extends('frontend.master')
@section('content')
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper align-items-center auth auth-bg-1 theme-one">
                @include('frontend.student.navbar.navbar')
                <div class="row">
                    <div class="col-lg-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Mark's of the Students</h4>
                                @if ($data==null)
                                    <div class="hed">
                                        <h1 class="text-center">You are not given any payment yet.</h1>
                                    </div>
                                @else
                                    @foreach($data as $datas)
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Total Amount
                                                </th>
                                                <th>
                                                    Description
                                                </th>
                                                <th>
                                                    Year
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {{$datas->total->type}}
                                                </td>
                                                <td>
                                                    {{$datas->total->amount}}
                                                </td>
                                                <td>
                                                    {{$datas->description}}
                                                </td>
                                                <td>
                                                    {{$datas->year}}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
@stop
