</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2019
              <a href="#" target="_blank">QSS</a>
              . All rights reserved.
            </span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
{{--Ajax Data Load Start--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js  "></script>
{{--Ajax Data Load End--}}
<script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>

<script src="{{asset('js/off-canvas.js')}}"></script>
<script src="{{asset('js/misc.js')}}"></script>

<script src="{{asset('js/jquery.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>--}}
{{--<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery-1.12.0.min.js')}}"><\/script>')</script>--}}
{{--<script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>--}}
{{--<script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>--}}
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- bootstrap js-->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
{{--<script src="{{asset('js/off-canvas.js')}}"></script>--}}
{{--<script src="{{asset('js/misc.js')}}"></script>--}}
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('js/dashboard.js')}}"></script>
<!-- End custom js for this page-->
<script type="text/javascript">
    // console.log("hmm its change");
    $(document).ready(function(){
        // Category Name Change Start
        $(document).on('change','#class_id',function(){
            var student_id=$(this).val();
            var div=$(this).parent();
            var paymentValue=" ";
            // console.log(student_id);
            $.ajax({
                type:'get',
                url:'{!!URL::to('jsonAddMarks')!!}',
                data:{'id':student_id},
                success:function(data){
                    // console.log(data.first_term!==null);
                    $("#ft_mrk").empty().append('<input type="text" class="form-control" id="paymentValue" value="'+data.first_term+'" name="first_term" placeholder="Amount">');
                    $("#mt_mrk").empty().append('<input type="text" class="form-control" id="paymentValue" value="'+data.mid_term+'" name="mid_term" placeholder="Amount">');
                    $("#fi_mrk").empty().append('<input type="text" class="form-control" id="paymentValue" value="'+data.final_term+'" name="final_term" placeholder="Amount">');
                    // if(data==null){
                    //     $("#ft_mrk").empty().append('<input type="number" class="form-control"  min="1" max="100" value="'+data.first_term+'" name="first_term" readonly>');
                    //     $("#mt_mrk").empty().append('<input type="number" class="form-control"  min="1" max="100" value="'+data.mid_term+'" name="first_term" readonly>');
                    //     $("#fi_mrk").empty().append('<input type="number" class="form-control"  min="1" max="100" value="'+data.final_term+'" name="first_term" readonly>');
                    // }else{
                    //     $("#ft_mrk").empty().append('<input type="number" class="form-control"  min="1" max="100" value=""  name="first_term">');
                    //     $("#mt_mrk").empty().append('<input type="number" class="form-control"  min="1" max="100" name="first_term">');
                    //     $("#fi_mrk").empty().append('<input type="number" class="form-control"  min="1" max="100"  name="first_term">');
                    // }
                },
                error:function(){
                }
            });
        });
        //Category Name Change End
    });
</script>
</body>

</html>
