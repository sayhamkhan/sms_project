<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered">
    <tr>
        <td>Name:
            {{$downloadPdf->student->first_name}}
            {{$downloadPdf->student->last_name}}
        </td>
        <td>Amount:
            {{$downloadPdf->total->amount}}
        </td>
    </tr>
    <tr>
        <td>Phone: {{$downloadPdf->description}}</td>
        <td>Email:{{$downloadPdf->year}}</td>
        <td>Gender: {{$downloadPdf->created_at}}</td>
    </tr>
</table>
</body>
</html>
