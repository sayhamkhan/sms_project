<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

///Route::get('/', function () {
//    return view('welcome');
//});
//Frontend Route List
Route::get('/', 'Frontend\HomeController@index')->name('index');
//Authentication Route List
Route::get('/teacher/login', 'Frontend\TeacherController@teacherLogin')->name('teacherLogin');
Route::post('/teacher/process', 'Frontend\TeacherController@teacherLoginProcess')->name('teacherLoginProcess');
Route::get('/logout', 'Frontend\HomeController@logout')->name('logout');
Route::get('/adminLogout', 'Frontend\HomeController@adminLogout')->name('adminLogout');
Route::get('/teacher/profile', 'Frontend\TeacherController@teacherProfile')->name('teacherProfile');
Route::get('/take/attendance', 'Frontend\TeacherController@takeAttendance')->name('takeAttendance');
Route::post('/take/attendanceProcess', 'Frontend\TeacherController@takeAttendanceProcess')->name('takeAttendanceProcess');
Route::get('/show/attendance', 'Frontend\TeacherController@showAttendance')->name('showAttendance');
Route::get('/teacherClassList', 'Frontend\TeacherController@teacherClassList')->name('teacherClassList');
Route::get('/selectClass', 'Frontend\TeacherController@selectClass')->name('selectClass');
Route::post('/selectClassProcess', 'Frontend\TeacherController@selectClassProcess')->name('selectClassProcess');
Route::get('/jsonAddMarks', 'Frontend\TeacherController@jsonAddMarks');
Route::put('/submitmarks', 'Frontend\TeacherController@submitmarks')->name('submitmarks');
Route::get('/submitMarksList', 'Frontend\TeacherController@submitMarksList')->name('submitMarksList');
//Student Route List
Route::get('/student/login', 'Frontend\StudentController@studentLogin')->name('studentLogin');
Route::post('/student/process', 'Frontend\StudentController@studentLoginProcess')->name('studentLoginProcess');
Route::get('/student/info', 'Frontend\StudentController@studentInfo')->name('studentInfo');
Route::get('/student/attendanceInfo', 'Frontend\StudentController@attendanceInfo')->name('attendanceInfo');
Route::get('/student/resultIfo', 'Frontend\StudentController@resultIfo')->name('resultIfo');
Route::get('/student/payment/info', 'Frontend\StudentController@paymentInfo')->name('paymentInfo');



//Backend Route List
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/adminIndex', 'UserController@adminIndex')->name('adminIndex')->middleware('auth:admin');
//Route::get('delete/{id}', 'HomeController@destroy')->name('destroy');
Route::get('/passwordChange', 'Auth\ChangePasswordController@index')->name('paasword.change');
Route::post('/passwordUpdated', 'Auth\ChangePasswordController@update')->name('password.update');
//Payments Route List
Route::get('/paymentsList', 'PaymentController@paymentsList')->name('paymentsList');
Route::post('/payType', 'PaymentController@payType')->name('payType');
Route::get('/createPayments', 'PaymentController@createPayments')->name('createPayments');
Route::get('/jsonAddPaymentValue', 'PaymentController@jsonAddPaymentValue');
Route::post('/createPaymentProcess', 'PaymentController@createPaymentProcess')->name('createPaymentProcess');
Route::get('/jsonAddPaymentValue','PaymentController@jsonAddPaymentValue')->middleware('auth:admin');
Route::post('/createPaymentProcess', 'PaymentController@createPaymentProcess')->name('createPaymentProcess');
Route::get('/payment/delete/{id}', 'PaymentController@paymentDelete')->name('paymentDelete');
//Salary Route List
Route::get('/salaryList', 'PaymentController@salaryList')->name('salaryList');
Route::get('/createSalary', 'PaymentController@createSalary')->name('createSalary');
Route::post('/createSalaryProcess', 'PaymentController@createSalaryProcess')->name('createSalaryProcess');
Route::get('/edit/salary/{id}', 'PaymentController@editSalary')->name('editSalary');
Route::post('/edit/salary/process/{id}', 'PaymentController@editSalaryProcess')->name('editSalaryProcess');
//Fee Route List
Route::get('/feeList', 'PaymentController@feeList')->name('feeList');
Route::post('/createFees', 'PaymentController@createFees')->name('createFees');
Route::get('/fee/delete/{id}', 'PaymentController@feeDelete')->name('feeDelete');
//Class Route List
Route::get('/classList', 'ClassController@classList')->name('classList');
Route::post('/createClasses', 'ClassController@createClasses')->name('createClasses');
Route::get('/editCls/{id}', 'ClassController@editCls')->name('editCls');
Route::put('editClassesProcess/{id}', 'ClassController@editClassesProcess')->name('editClassesProcess');
Route::get('clsDelete/{id}', 'ClassController@clsDelete')->name('clsDelete');
//Course Route List
Route::get('/courseList', 'ClassController@courseList')->name('courseList');
Route::post('/createCourse', 'ClassController@createCourse')->name('createCourse');
Route::get('courseDelete/{id}', 'ClassController@courseDelete')->name('courseDelete');
Route::get('update/{id}', 'ClassController@updateCourse')->name('updateCourse');
Route::put('editCourseProcess/{id}', 'ClassController@editCourseProcess')->name('editCourseProcess');
//Section Route List
Route::get('/sectionList', 'ClassController@sectionList')->name('sectionList');
Route::post('/createSection', 'ClassController@createSection')->name('createSection');
Route::get('secDelete/{id}', 'ClassController@secDelete')->name('secDelete');
Route::get('edit/{id}', 'ClassController@editSection')->name('editSection');
Route::put('editSectionProcess/{id}', 'ClassController@editSectionProcess')->name('editSectionProcess');
//Student Route List
Route::get('/studentList', 'UserController@studentList')->name('studentList');
Route::post('/createStudent', 'UserController@createStudent')->name('createStudent');
Route::get('/student/edit/{id}', 'UserController@editStudent')->name('editStudent');
Route::post('/student/edit/process/{id}', 'UserController@editStudentProcess')->name('editStudentProcess');
Route::get('/student/delete/{id}', 'UserController@deleteStudent')->name('deleteStudent');
//Teacher Route List
Route::get('/teacherList', 'UserController@teacherList')->name('teacherList');
Route::post('/createTeacher', 'UserController@createTeacher')->name('createTeacher');
Route::get('editTeacher/{id}', 'UserController@editTeacher')->name('editTeacher');
Route::put('editTeacherProcess/{id}', 'UserController@editTeacherProcess')->name('editTeacherProcess');
Route::get('remove/{id}', 'UserController@teacherDelete')->name('teacherDelete');
//Assign Route List
Route::get('/formMaster', 'ClassController@formMaster')->name('formMaster');
Route::post('/assignFormMaster', 'ClassController@assignFormMaster')->name('assignFormMaster');
Route::get('/classTeacher', 'ClassController@classTeacher')->name('classTeacher');
Route::post('/assignClassTeacher', 'ClassController@assignClassTeacher')->name('assignClassTeacher');
//Mark Publish Route List
Route::get('/markPublishList', 'UserController@markPublishList')->name('markPublishList');
Route::get('/publishMark/{id}', 'UserController@publishMark')->name('publishMark');

//PDF Route List
Route::get('teacherSalary', 'Frontend\TeacherController@teacherSalary')->name('teacherSalary');

//PDF Route List
Route::get('paymentPrint/{id}','UserController@paymentPrint')->name('paymentPrint');
Route::get('/customers/pdf','UserController@export_pdf');


//PDF Route List
Route::get('generate-pdf','UserController@generateStudentPDF')->name('studentPdf');
Route::get('/customers/pdf','UserController@export_pdf');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// print routes

Route::get('/students','PrintController@index');
Route::get('/prnpriview','PrintController@prnpriview');